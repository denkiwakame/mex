#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv/cxcore.h>
#include <mex.h>

namespace mexcv {
  inline cv::Mat mxArray2cvMat(const mxArray * src) {
    const int m = mxGetM(src);
    const int n = mxGetN(src);

    // dpubleのmxArray
    if(! mxIsDouble(src) ) {
      mexErrMsgTxt("Input array is not double\n");
    }
    const double * p = mxGetPr(src);

    cv::Mat dst = cv::Mat(m, n, CV_64FC1);
    for(int r=0 ; r<m ; r++) {
      for(int c=0 ; c<n ; c++) {
        dst.at<float>(r,c) = (float)p[c*m+r];
      }
    }
    return dst;
  }

// CV_8U - 8-bit unsigned integers ( 0..255 )
// CV_8S - 8-bit signed integers ( -128..127 )
// CV_16U - 16-bit unsigned integers ( 0..65535 )
// CV_16S - 16-bit signed integers ( -32768..32767 )
// CV_32S - 32-bit signed integers ( -2147483648..2147483647 )
// CV_32F - 32-bit floating-point numbers ( -FLT_MAX..FLT_MAX, INF, NAN )
// CV_64F - 64-bit floating-point numbers ( -DBL_MAX..DBL_MAX, INF, NAN )
  inline mxArray * cvMat2mxArray(const cv::Mat& src) {
    const int m = src.rows;
    const int n = src.cols;

    // convert to double
    // supports only CV_16U
    mxArray * dst = mxCreateDoubleMatrix(m, n, mxREAL);
    double * p = mxGetPr(dst);
    for(int r=0 ; r<m ; r++) {
      for(int c=0 ; c<n ; c++) {
        p[c*m+r] = (double)src.at<unsigned short>(r,c);
      }
    }
    return dst;
  }

  // from matlabroot/extern/examples/mx/mxcreatecharmatrixfromstr.c
  inline mxArray * char2mx(const char * str) {
    // FIXME exception: str => nonchar
    return mxCreateCharMatrixFromStrings(1, &str);
  }
}
