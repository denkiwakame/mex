[status, opencv_incdir] = system('pkg-config --cflags opencv');
[status, opencv_ldflags] = system('pkg-config --libs opencv');

opencv_incdir = opencv_incdir(1:end-3)
opencv_ldflags = opencv_ldflags(1:end-3)

TARGET_FILE='./src/readxml.cpp';
mex_compile_options = sprintf('mex CXX=g++ %s %s %s', opencv_incdir, opencv_ldflags, TARGET_FILE)
eval(mex_compile_options)
