#include "mex.h"
#include "../include/opencv_mex.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]){
  /***************************************************
   * [corresponding points] = * output: 	corresponding points array
   * input: XML file name
   *
   * **************************************************/
  if (nrhs != 2){
    mexPrintf("input arguments should be 1\n");
    return;
  }

  /* CHESSBOARD IMAGE FILE */
  const char *fname = mxArrayToString(prhs[0]);
  mexPrintf("loading ... %s\n", fname);
  const char *nodename = mxArrayToString(prhs[1]);

  cv::Mat data;
  cv::FileStorage cvfs(fname, CV_STORAGE_READ);
  // failed to open or load_flag not set
  if (!cvfs.isOpened()) {
    mexPrintf("[ERROR]failed to open %s\n", fname);
    return;
  }

  mexPrintf("%s was loaded successfully.\n", fname);
  mexPrintf("getting node [%s]...\n", nodename);
  cv::FileNode node(cvfs.fs, NULL); // Get Top Node
  cv::read(node[std::string(nodename)], data);

  if (data.empty()) {
    mexPrintf("[ERROR] no such nodes: [%s]\n", nodename);
    return;
  }

  mxArray* mxarray_mat = mexcv::cvMat2mxArray(data);
  plhs[0] = mxarray_mat;
}

